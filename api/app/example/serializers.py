"""
Пример документирования сериалайзеров.
"""
from drf_yasg.utils import swagger_serializer_method
from rest_framework import serializers


class SomeQuerySerializer(serializers.Serializer):
    some_query_field = serializers.CharField(max_length=50)


class SomeBodySerializer(serializers.Serializer):
    some_body_field = serializers.CharField(max_length=50)


class SomeOtherEntitySerializer(serializers.Serializer):
    some_int_field = serializers.SerializerMethodField(required=False)
    some_url_field = serializers.SerializerMethodField(required=False)

    def get_some_int_field(self, obj) -> int:
        return 0

    @swagger_serializer_method(serializers.URLField())
    def get_some_url_field(self, obj):
        return 'https://some-url.com'


class ExampleEntitySerializer(serializers.Serializer):
    example_field = serializers.SerializerMethodField(required=False)

    @swagger_serializer_method(SomeOtherEntitySerializer(many=True))
    def get_example_field(self, obj):
        return [SomeOtherEntitySerializer() for _ in range(5)]
